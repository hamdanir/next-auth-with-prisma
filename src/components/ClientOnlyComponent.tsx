// components/ClientOnlyComponent.tsx
// @next/client

import { useEffect } from "react";
import { signIn, useSession } from "next-auth/react";

function ClientOnlyComponent() {
  const { data: session } = useSession();

  useEffect(() => {
    // Your useEffect logic here
    // For example, redirecting after login
    if (!session) {
      // Redirect to login page if session doesn't exist
      signIn();
    }
  }, [session]);

  return null; // This component doesn't render anything
}

export default ClientOnlyComponent;
